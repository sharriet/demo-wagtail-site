from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel

from wagtail.images.models import Image
from wagtail.images.edit_handlers import ImageChooserPanel


class HomePage(Page):
    # define basic text field
    heading = models.TextField(blank=True)

    # define a rich text field
    body = RichTextField(blank=True)

    # define an image field
    banner = models.ForeignKey(
            'wagtailimages.Image',
            null=True,
            blank=True,
            on_delete=models.SET_NULL,
            related_name='+'
    )
    
    # create the panels
    content_panels = Page.content_panels + [
            FieldPanel('heading', classname="full"),
            FieldPanel('body', classname="full"),
            ImageChooserPanel('banner'),
    ]
    pass

class ContentPage(Page):
    # define basic text field
    heading = models.TextField(blank=True)

    # define a rich text field
    body = RichTextField(blank=True)

    # define an image field
    main_img = models.ForeignKey(
            'wagtailimages.Image',
            null=True,
            blank=True,
            on_delete=models.SET_NULL,
            related_name='+'
    )
    
    # create the panels
    content_panels = Page.content_panels + [
            FieldPanel('heading', classname="full"),
            FieldPanel('body', classname="full"),
            ImageChooserPanel('main_img'),
    ]
    pass
